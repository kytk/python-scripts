#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# A script to fill PatientID with the name of source directory
# Part of this script is based on the script provided by Yuya Saito

# 07 Sep 2022 K. Nemoto
 
import sys, os, time, re, shutil, argparse, subprocess
import pydicom
 
__version__ = '1.0 (2022/09/07)'
 
__desc__ = '''
Fill PatientID with the name of the input directory
'''
__epilog__ = '''
examples:
  dcm_fillid.py DICOM_DIR
'''

def fill_id(src_dir):
    # modify files
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            try:
                src_file = os.path.join(root, file)
                ds = pydicom.dcmread(src_file)
                pid = src_dir.replace('/','')
                print('Change ID: {} -> {}'.format(ds.PatientID,pid))
                ds.PatientName = pid
                ds.PatientID = pid
                ds.save_as(src_file)
            except:
                pass


if __name__ == '__main__':
    start_time = time.time()
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('dirs', metavar='DICOM_DIR', help='DICOM directory.', nargs=1)
 
    err = 0
    try:
        args = parser.parse_args()
        print(args.dirs[0])
        fill_id(args.dirs[0])
        print("execution time: %.2f second." % (time.time() - start_time))
    except Exception as e:
        print("%s: error: %s" % (__file__, str(e)))
        err = 1
 
    sys.exit(err)
